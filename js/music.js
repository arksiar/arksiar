$(document).ready(function(){
	var audios=$("#music")[0];
	var a=true;
	var b=true;
	var c=0;
	//播放按钮控制
	$("#playBTN").click(function(){
	    if(a==false){
	        audios.play();
		    a=true;
		    $("#playBTN").attr("src","img/播放.png");
		    
	    }else{
		    audios.pause();
		    a=false;
		    $("#playBTN").attr("src","img/暂停.png");
		    
	    }
	});
	//音量按钮控制
	$("#sound").click(function(){
	    if(b==true){
		    b=false;
		    $("#sound").attr("src","img/静音.png");
		    $(".drag_btn").css({"left":0}); 
			audios.volume=0;
	    }else{
		    b=true;
		    $(".drag_btn").css({"left":100}); 
			audios.volume=1;
		    $("#sound").attr("src","img/声音.png")
	    }
	});
	//下一曲
	$("#nextSong").click(function(){
		c++;
		if(c==1){
			$("audio").attr("src","music/我们都一样 - 曹轩宾.mp3");
			$("#playPic2").attr("src","gif/playing.gif");
			$("#playPic1").attr("src","gif/m.gif");
			$("#playPic3").attr("src","gif/m.gif");
		}else if(c==2){
			$("audio").attr("src","music/张碧晨 - 年轮.mp3");
			$("#playPic3").attr("src","gif/playing.gif");
			$("#playPic2").attr("src","gif/m.gif");
			$("#playPic1").attr("src","gif/m.gif");
		}else if(c==3){
			$("audio").attr("src","music/逃脱 - 阿悄.mp3");
			$("#playPic1").attr("src","gif/playing.gif");
			$("#playPic2").attr("src","gif/m.gif");
			$("#playPic3").attr("src","gif/m.gif");
			c=0;
		}else if(c==-1){
			$("audio").attr("src","music/逃脱 - 阿悄.mp3");
			$("#playPic1").attr("src","gif/playing.gif");
			$("#playPic2").attr("src","gif/m.gif");
			$("#playPic3").attr("src","gif/m.gif");
		}else if(c==-2){
			$("audio").attr("src","music/张碧晨 - 年轮.mp3");
			$("#playPic3").attr("src","gif/playing.gif");
			$("#playPic2").attr("src","gif/m.gif");
			$("#playPic1").attr("src","gif/m.gif");
		}else if(c==-3){
			$("audio").attr("src","music/我们都一样 - 曹轩宾.mp3");
			$("#playPic2").attr("src","gif/playing.gif");
			$("#playPic1").attr("src","gif/m.gif");
			$("#playPic3").attr("src","gif/m.gif");
			c=0;
		}
	});
	//上一曲
	$("#lastSong").click(function(){
		c--;
		if(c==1){
			$("audio").attr("src","music/我们都一样 - 曹轩宾.mp3");
			$("#playPic2").attr("src","gif/playing.gif");
			$("#playPic1").attr("src","gif/m.gif");
			$("#playPic3").attr("src","gif/m.gif");
		}else if(c==2){
			$("audio").attr("src","music/张碧晨 - 年轮.mp3");
			$("#playPic3").attr("src","gif/playing.gif");
			$("#playPic2").attr("src","gif/m.gif");
			$("#playPic1").attr("src","gif/m.gif");
		}else if(c==3){
			$("audio").attr("src","music/逃脱 - 阿悄.mp3")
			$("#playPic1").attr("src","gif/playing.gif");
			$("#playPic2").attr("src","gif/m.gif");
			$("#playPic3").attr("src","gif/m.gif");;
			c=0;
		}else if(c==-1){
			$("audio").attr("src","music/张碧晨 - 年轮.mp3");
			$("#playPic3").attr("src","gif/playing.gif");
			$("#playPic2").attr("src","gif/m.gif");
			$("#playPic1").attr("src","gif/m.gif");
		}else if(c==-2){
			$("audio").attr("src","music/我们都一样 - 曹轩宾.mp3");
			$("#playPic2").attr("src","gif/playing.gif");
			$("#playPic1").attr("src","gif/m.gif");
			$("#playPic3").attr("src","gif/m.gif");
		}else if(c==-3){
			$("audio").attr("src","music/逃脱 - 阿悄.mp3")
			$("#playPic1").attr("src","gif/playing.gif");
			$("#playPic2").attr("src","gif/m.gif");
			$("#playPic3").attr("src","gif/m.gif");;
			c=0;
		}
	});
	//音量纽扣调节控制
	var move=false;//移动标记 
	var _x,_px;//鼠标离控件左上角的相对位置 
	$(".drag_btn").mousedown(function(e){ 
		move=true; 
		_x=e.pageX-parseInt($(".drag_btn").css("left")); 
		
	}); 
	$(".drag_btn").mousemove(function(e){ 
		if(move==true){ 
			var x=e.pageX-_x;
			if(x>0&&x<=100)
			{
				$(".drag_btn").css({"left":x}); 
				audios.volume=x/100;
				$("#sound").attr("src","img/声音.png");
				b=true;
			}else if(x==0){
				$(".drag_btn").css({"left":x}); 
				audios.volume=0;
				$("#sound").attr("src","img/静音.png");
				b=false;
			}
		} 
	});
	$(".drag_btn").mouseup(function(e){
		move=false;
	});
	$(".drag_btn").mouseleave(function(e){
		move=false;
	});
	//音量条直接点击控制
	$(".controlVoice").click(function(e){
		
		px=e.pageX-parseInt($(".controlVoice").offset().left); 
		
		if(px>0&&px<=100)
			{
				$(".drag_btn").css({"left":px}); 
				audios.volume=px/100;
				$("#sound").attr("src","img/声音.png");
				b=true;
			}else if(x==0){
				$(".drag_btn").css({"left":px}); 
				audios.volume=0;
				$("#sound").attr("src","img/静音.png");
				b=false;
			}
	});
	//歌曲列表按钮控制
	var k=true;
	$("#music_list").click(function(){
		if(k==true)
		{
			$("#song_list").stop().slideDown();
			k=false; 
		}
		else if(k==false)
		{
			$("#song_list").stop().slideUp();
			k=true;
		}
	});
	$("#song_list1").click(function(){
		$("audio").attr("src","music/逃脱 - 阿悄.mp3");
		$("#playPic1").attr("src","gif/playing.gif");
		$("#playPic2").attr("src","gif/m.gif");
		$("#playPic3").attr("src","gif/m.gif");
		
		c=0;
	});
	$("#song_list2").click(function(){
		$("audio").attr("src","music/我们都一样 - 曹轩宾.mp3");
		$("#playPic2").attr("src","gif/playing.gif");
		$("#playPic1").attr("src","gif/m.gif");
		$("#playPic3").attr("src","gif/m.gif");
		c=1;
	});
	$("#song_list3").click(function(){
		$("audio").attr("src","music/张碧晨 - 年轮.mp3");
		$("#playPic3").attr("src","gif/playing.gif");
		$("#playPic2").attr("src","gif/m.gif");
		$("#playPic1").attr("src","gif/m.gif");
		c=2;
	});
	//播放条控制
	
	setInterval(function(){
		$("#songTime").click(function(e){
			tx=e.pageX-parseInt($("#songTime").offset().left); 
			var timeClick=tx/$("#songTime").width();
			audios.currentTime= parseInt(audios.duration*timeClick);
			$("#timeBlack").css("width",tx);
		});
		var time=audios.currentTime/audios.duration;
		var black=$("#songTime").width()*time;
		$("#timeBlack").css("width",black);
		if(audio.ended){
			$("#timeBlack").css("width",0);
		}
		
	},1);
	
	//判断音乐是否加载完全
	if(audios.ended)
	{
		c++;
		if(c==1){
			$("audio").attr("src","music/我们都一样 - 曹轩宾.mp3");
			$("#playPic2").attr("src","gif/playing.gif");
			$("#playPic1").attr("src","gif/m.gif");
			$("#playPic3").attr("src","gif/m.gif");
		}else if(c==2){
			$("audio").attr("src","music/张碧晨 - 年轮.mp3");
			$("#playPic3").attr("src","gif/playing.gif");
			$("#playPic2").attr("src","gif/m.gif");
			$("#playPic1").attr("src","gif/m.gif");
		}else if(c==3){
			$("audio").attr("src","music/逃脱 - 阿悄.mp3");
			$("#playPic1").attr("src","gif/playing.gif");
			$("#playPic2").attr("src","gif/m.gif");
			$("#playPic3").attr("src","gif/m.gif");
			c=0;
		}else if(c==-1){
			$("audio").attr("src","music/逃脱 - 阿悄.mp3");
			$("#playPic1").attr("src","gif/playing.gif");
			$("#playPic2").attr("src","gif/m.gif");
			$("#playPic3").attr("src","gif/m.gif");
		}else if(c==-2){
			$("audio").attr("src","music/张碧晨 - 年轮.mp3");
			$("#playPic3").attr("src","gif/playing.gif");
			$("#playPic2").attr("src","gif/m.gif");
			$("#playPic1").attr("src","gif/m.gif");
		}
	}
	
	
})
